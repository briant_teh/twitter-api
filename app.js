require('dotenv').config()

const express = require('express');
// const path = require('path');
const logger = require('morgan');

const twitterApiRoute = require('./routes/twitter-api');

const app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', process.env.DASHBOARD_ORIGIN);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-requested-With, content-type');

    next();
});

app.use('/twitter-api', twitterApiRoute);

module.exports = app;