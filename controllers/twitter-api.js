const Twitter = require('twitter');
const request = require('request');

const client = new Twitter({
    consumer_key: process.env.TWITTER_CONSUMER_KEY,
    consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
    access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
    access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
});

exports.getRequest = (req, res) => {
    const query = req.query;
    let valid = true;
    let errMsg = [];

    if (!query.path) {
        valid = false;
        errMsg.push(`'path' not specified in query`);
    }

    console.log('query.params', query.params);

    if (!query.params) {
        valid = false;
        errMsg.push(`'params' not specified in query`);
    } else {
        try {
            JSON.parse(query.params)
        } catch (err) {
            valid = false;
            errMsg.push(`'params' is an invalid JSON input`);
        }
    }

    if (!valid) {
        res.status(400).json({
            success: false,
            err: {
                message: errMsg
            }
        })
    } else {
        client.get(query.path, JSON.parse(query.params), (err, tweets, results) => {
            if (!err) {
                res.status(200).json(tweets);
            } else {
                res.status(500).json(err);
            }
        });
    }
}

exports.postRequest = (req, res) => {
    client.get(req.body.path, req.body.params, (err, tweets, results) => {
        if (!err) {
            res.status(200).json(tweets);
        } else {
            res.status(500).json(err);
        }
    });
}

exports.searchTweets = (req, res) => {
    const client = new Twitter({
        consumer_key: process.env.TWITTER_CONSUMER_KEY,
        consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
        access_token_key: req.body.access_token,
        access_token_secret: req.body.access_token_secret
    });
    
    client.get('search/tweets', req.body, (err, tweets, results) => {
        if (!err) {
            res.status(200).json(tweets);
        } else {
            res.status(500).json(err);
        }
    });
}

exports.statusesOembed = (req, res) => {
    const options = {
        method: 'get',
        url: `https://publish.twitter.com/oembed?url=${req.query.url}`
    }

    request(options, (err, response, data) => {
        if (!err) {
            res.status(200).json(data);
        } else {
            res.status(500).json(err);
        }
    });
}

exports.oauthRequestToken = (req, res) => {
    const options = {
        // headers: {
        //     Accept: '*/*',
        //     Connection: 'close',
        //     'User-Agent': 'node-twitter/1'
        // },
        oauth: {
            consumer_key: process.env.TWITTER_CONSUMER_KEY,
            consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
            token: process.env.TWITTER_ACCESS_TOKEN_KEY,
            token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
            callback: process.env.CALLBACK_URL
        },
        method: 'post',
        url: 'https://api.twitter.com/oauth/request_token'
        // form: {
        //     'oauth_callback': process.env.CALLBACK_URL
        // }
    };

    request(options, (err, response, data) => {
        if (!err) {
            res.status(200).json(data);
        } else {
            res.status(500).json(err);
        }
    });
}

exports.oauthAccessToken = (req, res) => {
    console.log('req.body', req.body);
    console.log('JSON req.body', JSON.stringify(req.body));
    const options = {
        oauth: {
            consumer_key: process.env.TWITTER_CONSUMER_KEY,
            consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
            token: req.body.request_token,
            token_secret: req.body.request_token_secret,
            callback: process.env.CALLBACK_URL
        },
        method: 'post',
        url: 'https://api.twitter.com/oauth/access_token?oauth_verifier=' + req.body.oauth_verifier
    };

    request(options, (err, response, data) => {
        if (!err) {
            console.log('data', data);
            res.status(200).json(data);
        } else {
            res.status(500).json(err);
        }
    });
}