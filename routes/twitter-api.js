const express = require('express')
const router = express.Router();

const twitterApiController = require('../controllers/twitter-api');

router.get('/', twitterApiController.getRequest);
router.get('/oembed', twitterApiController.statusesOembed);
router.post('/', twitterApiController.postRequest);
router.post('/search/tweets', twitterApiController.searchTweets);
router.post('/oauth/request-token', twitterApiController.oauthRequestToken);
router.post('/oauth/access-token', twitterApiController.oauthAccessToken);

module.exports = router;