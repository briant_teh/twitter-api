const app = require('./app');
const http = require('http');

var port = process.env.PORT;
console.log('port', port);

// app.set(port);

const server = http.createServer(app);
server.listen(port, () => {
    console.log('Server listening on port...', port);
});
